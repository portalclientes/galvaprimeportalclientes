﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';
import axios from 'axios';

export interface SeguimientoOCStoreState {
    isloading: boolean,
    registros: any[]
}

export interface RequestInformacionInicial {
    type: 'REQUEST_INFORMACION_INICIAL'
}

export interface ReceiveInformacionInicial {
    type: 'RECEIVE_INFORMACION_INICIAL',
    registros: any[]
}

type KnownAction = RequestInformacionInicial | ReceiveInformacionInicial;

export const actionCreators = {
    requestInformacionInicial: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        dispatch({ type: 'REQUEST_INFORMACION_INICIAL'});
        var respnseA = axios.post('http://saturno.jaguarux.com/apifcligalvap/api/DetallesOV/Resumen', {})
            .then(response => response.data)
            .then(data => {
                console.log(data);
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL', registros: data.OData });
            })
            .catch(error => {
                dispatch({ type: 'RECEIVE_INFORMACION_INICIAL', registros: [] });
                console.log(error);
            })
    },
    requestDowloadExcel: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        //dispatch({ type: 'REQUEST_INFORMACION_INICIAL' });
        //var respnseA = axios.post('http://saturno.jaguarux.com/apifcligalvap/api/DetallesOV/Excel', {})
        //    .then(response => response.data)
        //    .then(data => {
        //        console.log(data);
        //        const url = window.URL.createObjectURL(new Blob([data]));
        //        const link = document.createElement('a');
        //        link.href = url;
        //        link.setAttribute('download', 'Orden de venta.xls');
        //        document.body.appendChild(link);
        //        link.click();
        //    })
        //    .catch(error => {
        //        console.log(error);
        //    })

        window.location.href = "http://saturno.jaguarux.com/apifcligalvap/api/DetallesOV/ExcelForm";
    }
};

const unloadedState: SeguimientoOCStoreState = {
    isloading: false,
    registros: []
};

export const reducer: Reducer<SeguimientoOCStoreState> = (state: SeguimientoOCStoreState | undefined, incomingAction: Action): SeguimientoOCStoreState => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'REQUEST_INFORMACION_INICIAL':
            return {
                ...state,
                isloading: true,
                registros: []
            };
        case 'RECEIVE_INFORMACION_INICIAL':
            return {
                ...state,
                isloading: false,
                registros: action.registros
            };
        default:
            return state;
    }
};
