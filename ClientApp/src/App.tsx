import * as React from 'react';
import { Route, Switch } from 'react-router';
import LogIn from './components/common/LogIn';
import Home from './components/Home';
import SeguimientoOC from './components/pages/SeguimientoOC';
import SeguimientoOCDetalle from './components/pages/SeguimientoOCDetalle';
import Layout from './components/Layout';
export default () => (
    
    <Switch>
        <Route exact path='/' component={LogIn} />
        <Layout>
            <Route path='/home' component={Home} />
            <Route path='/SeguimientoOC' component={SeguimientoOC} />
            <Route path='/SeguimientoOCDetalle' component={SeguimientoOCDetalle} />
        </Layout>
    </Switch>
  
);
