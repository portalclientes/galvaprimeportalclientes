﻿import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Toast, ToastBody, ToastHeader } from 'reactstrap';

interface LogInState {
	email: string,
	password: string
}
export default class LogIn extends React.Component<RouteComponentProps<{}>, LogInState>{
	constructor(props: any) {
		super(props);
		this.state = {
			email: "",
			password: ""

		};
    }
	public LogIn = () => {
		if (this.state.email == "facturacion@galvaprime.com" && this.state.password == "123") {
			//this.props.history.replace("/home");
			window.location.replace("/home");
		} else {
			alert("Usuario y contraseña incorrecto");
        }
	}
    public render() {
        return <section className="login p-fixed d-flex text-center bg-primary common-img-bg">
	<div className="container-fluid">
		<div className="row">
			<div className="col-sm-12">
				<div className="login-card card-block">
					<form className="md-float-material">
								<div className="text-center">
									<img src={'/images/Logo-Brand.png'} alt="logo" style={{ width:'50%' }} />
						</div>
								<h3 className="text-center text-muted">
							Inicia sesión en tu cuenta
						</h3>
						<div className="row">
							<div className="col-md-12">
								<div className="md-input-wrapper">
											<input type="email" className="md-form-control"
												onChange={(evt: any) => { this.setState({ email: evt.target.value }); }}
												required />
									<label>Correo electrónio</label>
								</div>
							</div>
							<div className="col-md-12">
								<div className="md-input-wrapper">
											<input type="password" className="md-form-control"
												onChange={(evt: any) => { this.setState({ password: evt.target.value }); }}
												required />
									<label>Contraseña</label>
								</div>
							</div>
							<div className="col-sm-6 col-xs-12">
								<div className="rkmd-checkbox checkbox-rotate checkbox-ripple m-b-25">
									<label className="input-checkbox checkbox-primary">
										<input type="checkbox" id="checkbox" />
										<span className="checkbox"></span>
									</label>
									<div className="captions">Recordar contraseña</div>

								</div>
							</div>
							<div className="col-sm-6 col-xs-12 forgot-phone text-right">
								<a href="forgot-password.html" className="text-right f-w-600"> ¿Olvido su contraseña?</a>
							</div>
						</div>
						<div className="row">
									<div className="col-xs-10 offset-xs-1">
										<button type="button" onClick={this.LogIn} className="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Iniciar sesión</button>
							</div>
						</div>
						<div className="col-sm-12 col-xs-12 text-center">
							<span className="text-muted">¿No tienes una cuenta? </span>
							<a href="register2.html" className="f-w-600 p-l-5">Regístrese ahora</a>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</section>
    }
}