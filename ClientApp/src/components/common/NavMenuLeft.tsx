﻿import * as React from 'react';
import { Link } from 'react-router-dom';
import { NavLink } from 'reactstrap';
import { RouteComponentProps } from 'react-router';
export default class NavMenuLeft extends React.Component<any, any>{
    public render() {
        return (
            <aside className="main-sidebar hidden-print ">
                <section className="sidebar" id="sidebar-scroll">
                    <ul className="sidebar-menu">
                        <li className="treeview">
                            <a className="waves-effect waves-dark" href="#">
                                <i className="icon-briefcase"></i>
                                <span> Consultas</span>
                                <i className="icon-arrow-down"></i>
                            </a>
                            <ul className="treeview-menu">
                                <li>
                                    <NavLink tag={Link} to="/SeguimientoOC"> <i className="icon-briefcase"></i> Seguimiento a OC</NavLink>
                                </li>
                                <li>
                                    <NavLink tag={Link} to="/home"> <i className="icon-briefcase"></i> Inventarios</NavLink>
                                </li>
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Embarques</a></li>
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Estado de cuenta</a></li>
                            </ul>
                        </li>
                        <li className="treeview">
                            <a className="waves-effect waves-dark" href="#">
                                <i className="icon-briefcase"></i>
                                <span> Documentos</span>
                                <i className="icon-arrow-down"></i>
                            </a>
                            <ul className="treeview-menu">
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Facturas</a></li>
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Remisiones</a></li>
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Certificados</a></li>
                            </ul>
                        </li>
                        <li className="treeview">
                            <a className="waves-effect waves-dark" href="#">
                                <i className="icon-briefcase"></i>
                                <span> Calidad</span>
                                <i className="icon-arrow-down"></i>
                            </a>
                            <ul className="treeview-menu">
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Fichas tecnicas</a></li>
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Reclamos</a></li>
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Certificaciones ISO</a></li>
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Garantias</a></li>
                            </ul>
                        </li>
                        <li className="treeview">
                            <a className="waves-effect waves-dark" href="#">
                                <i className="icon-briefcase"></i>
                                <span> Acerca de Galvaprime</span>
                                <i className="icon-arrow-down"></i>
                            </a>
                            <ul className="treeview-menu">
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Contactos</a></li>
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Terminos y condiciones</a></li>
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Aviso de privacidad</a></li>
                                <li><a className="waves-effect waves-dark" href="#"><i className="icon-briefcase"></i> Conózcanos</a></li>
                            </ul>
                        </li>

                    </ul>
                </section>
            </aside>
            );
    }
}