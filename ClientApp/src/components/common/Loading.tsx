﻿import * as React from 'react';
interface LoadingProps {
    mensage?: string
}
export default class Loading extends React.Component<LoadingProps, any>{
    public render() {
        return <div className="loader-bg">
            <div className="text-center" style={{ marginTop: '25%' }}>{this.props.mensage}</div>
            <div className="loader-bar"></div>
        </div>;
    }
}