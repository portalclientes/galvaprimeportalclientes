import * as React from 'react';
import NavMenuLeft from './common/NavMenuLeft';
import { RouteComponentProps } from 'react-router';
export default class Layout extends React.Component<any, any>{
    componentDidMount() {
        var menu: any = document.getElementById("menutop");
        menu.style.display = "block"
    }
    public render() {
        return <div>
            <NavMenuLeft />
            <div className="content-wrapper">
                <div className="container-fluid">
                    <div className="row">
                        <div className="main-header" style={{ marginRight: '10px' }}>
                            {this.props.children}
                        </div>
                    </div>

                </div>
            </div>

        </div>;
    }
}
