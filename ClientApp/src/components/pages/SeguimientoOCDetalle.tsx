﻿import * as React from 'react';
import * as numeral from 'numeral';
import * as _ from 'lodash';
import ReactTable from 'react-table';
import './css/react-table.css';
import { Card, Button, CardBody, CardText, CardHeader } from 'reactstrap';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../../store';
import * as SeguimientoOCDetalleStore from '../../store/SeguimientoOCDetalleStore';
import Loading from '../common/Loading';

type SeguimientoOCDetalleProps =
    SeguimientoOCDetalleStore.SeguimientoOCDetalleStoreState &
    typeof SeguimientoOCDetalleStore.actionCreators &
    RouteComponentProps<{}>;

interface SeguimientoOCDetalleState{
    vencimiento: string
}
class SeguimientoOCDetalle extends React.Component<SeguimientoOCDetalleProps, SeguimientoOCDetalleState>{
    constructor(props: any) {
        super(props);
        this.state = {
            vencimiento: ""

        };
    }
    componentDidMount() {
        const state = (this.props.location as any).state;
        this.setState({ vencimiento: state.vencimiento });
        this.props.requestInformacionInicial(state.vencimiento);
        console.log("state", state);
    }
    componentWillReceiveProps(props: any) {
        
    }
    public render() {
        const columns = [
            {
                Header: 'Periodo',
                accessor: 'Vencimiento',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'OC',
                accessor: 'OrderNum',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'Num Parte',
                accessor: 'llave',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'Codigo',
                accessor: 'ItemCode',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'Tipo Mat',
                accessor: 'MaterialType',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'Calibre',
                accessor: 'calibre',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'Ancho',
                accessor: 'ancho',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'Largo',
                accessor: 'largo',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'UDM',
                accessor: 'ven',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'Solicitado',
                accessor: 'OrderQty',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'Embarcado',
                accessor: 'DeliveredQty',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'Saldo',
                accessor: 'OpenQty',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'Producto Terminado',
                accessor: 'PTstock',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'Por Empaque',
                accessor: 'PTstockAsigned',
                Cell: (row: any) => {
                    return row.value;
                }
            },
            {
                Header: 'En Programa',
                accessor: 'PTstockAsigned',
                Cell: (row: any) => {
                    return row.value;
                }
            }
        ];
        return (this.props.isloading ? <Loading mensage={"Procesando por favor espere..."} />: < div >
            <Card >
                <CardHeader >
                    <Button
                        style={{ float: 'right' }}
                        className="btn btn-success btn-sm waves-effect text-center"
                        onClick={this.props.requestDowloadExcel}
                    >Excel
                            </Button>
                </CardHeader>
                <CardBody>
                    <ReactTable
                        data={this.props.registros}
                        columns={columns}
                       />
                </CardBody>
            </Card>
        </div>);
    }
}
export default connect(
    (state: ApplicationState) => state.SeguimientoOCDetalleStore, SeguimientoOCDetalleStore.actionCreators
)(SeguimientoOCDetalle as any);