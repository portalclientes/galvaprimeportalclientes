﻿
import * as React from 'react';
import * as numeral from 'numeral';
import * as _ from 'lodash';
import { Select, Button} from '@material-ui/core';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { ApplicationState } from '../../store';
import * as SeguimientoOCStore from '../../store/SeguimientoOCStore';
import { Bar } from 'react-chartjs-2';

import { Card, CardBody, CardText, CardHeader, Row, Col } from 'reactstrap';
import Loading from '../common/Loading';

type SeguimientoOCProps =
    SeguimientoOCStore.SeguimientoOCStoreState &
    typeof SeguimientoOCStore.actionCreators &
    RouteComponentProps<{}>;

class SeguimientoOC extends React.Component<SeguimientoOCProps>{

    componentDidMount() {
        this.props.requestInformacionInicial();
    }
    public render() {
        const data = this.props.registros.map((item: any) => {
            return item.OpenQty;
        });
        const labels = this.props.registros.map((item: any) => {
            return item.Vencimiento;
        });
        const props = this.props;
        return (
            this.props.isloading ? <Loading mensage={"Procesando por favor espere..."} /> :
                <div>
                    <Card >
                        <CardHeader >
                            <button style={{ float: 'right' }} className="btn waves-effect waves-light pulse" type="submit" name="action">Buscar</button>
                        </CardHeader>
                        <CardBody>
                            <Button variant="contained">Default</Button>
                            <Button variant="contained" color="primary">
                                Primary
      </Button>
                            <Button variant="contained" color="secondary">
                                Secondary
      </Button>
                            <Button variant="contained" disabled>
                                Disabled
      </Button>
                            <Button variant="contained" color="primary" href="#contained-buttons">
                                Link
      </Button>
                            <Row>
                                <Col lg={6} md={6} sm={12} xs={12}>
                                    <select className="form-control">
                                        <option value="0" selected>Selecciona un cliente</option>
                                        <option value="1">Cliente 1</option>
                                        <option value="2">Cliente 2</option>
                                        <option value="3">Cliente 3</option>
                                    </select>
                                </Col>
                                <Col lg={6} md={6} sm={12} xs={12}>
                                    <select className="form-control">
                                        <option value="0" selected>Selecciona un vendedor</option>
                                        <option value="1">Vendedor 1</option>
                                        <option value="2">Vendedor 2</option>
                                        <option value="3">Vendedor 3</option>
                                    </select>
                                </Col>
                            </Row>
                            <br />
                        </CardBody>
                    </Card>
                    <Card >
                        <CardHeader >
                            <Button
                                style={{ float: 'right', marginRight:'5px' }}
                                className="btn btn-secondary btn-sm waves-effect text-center"
                                onClick={() => { }}
                            >LB
                            </Button>
                            <Button
                                style={{ float: 'right', marginRight: '5px' }}
                                className="btn btn-success btn-sm waves-effect text-center"
                                onClick={this.props.requestDowloadExcel}
                            >Excel
                            </Button>
                        </CardHeader>
                        <CardBody>
                            <Bar
                                height={500}
                                width={500}
                                type='bar'
                                data={{
                                    labels: labels,
                                    datasets: [{
                                        label: 'Ordenes de compras abiertas',
                                        data: data,
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.2)',
                                            'rgba(54, 162, 235, 0.2)',
                                            'rgba(255, 206, 86, 0.2)',
                                            'rgba(75, 192, 192, 0.2)'
                                        ],
                                        borderColor: [
                                            'rgba(255, 99, 132, 1)',
                                            'rgba(54, 162, 235, 1)',
                                            'rgba(255, 206, 86, 1)',
                                            'rgba(75, 192, 192, 1)'
                                        ],
                                        borderWidth: 1
                                    }]
                                }}
                                options={
                                    {
                                        maintainAspectRatio: false,
                                        responsive: false,
                                        scales: {
                                            yAxes: [{
                                                ticks: {
                                                    beginAtZero: true
                                                }
                                            }]
                                        },
                                        onClick: function (e, items: any) {
                                            var index = items[0]._index;
                                            const vencimient = props.registros[index];
                                            props.history.push({
                                                pathname: `/SeguimientoOCDetalle`,
                                                state: {
                                                    vencimiento: vencimient ? vencimient.Vencimiento : "Adelantos"
                                                }
                                            });
                                        }
                                    }
                                }

                            />
                         </CardBody>
                    </Card>
                    
                </div>
            );
    }
}

export default connect(
    (state: ApplicationState) => state.SeguimientoOCStore, SeguimientoOCStore.actionCreators
)(SeguimientoOC as any);